#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>  
  
int main(void)
{
// 8 Bit PWM
 DDRB   |= (1 << PB2);                   // PWM output on PB2
 TCCR0A = (0 << COM0A1) | (1 << COM0A0) |(1 << WGM00) | (1 << WGM01);  // Fast PWM mode
 OCR0A  = 1;                          // initial PWM pulse width   
 TCCR0B = (1 << CS00) |  (0 << CS01) | (0 << CS02) | (1 << WGM02);   // clock source = CLK

//  16 Bit PWM
DDRB   |= (1 << PB3);                   // PWM output on PB3
TCCR1A = (0 << COM1A1) |(1 << COM1A0) | (1 << WGM11) | (1 << WGM10); 	// phase correct PWM, 10-bit
OCR1A = 1;                         	// initial PWM pulse width
TCCR1B = (1 << CS10) | (0 << CS11) | (1 << CS12) | (0 << WGM12) | (1 << WGM13);  // clock source = clock / 2046 ; start PWM


// input pins
DDRD &= ~(1 << PD6);    // PD6 is Input
PORTD |= (1 << PD6);    // enable pull-up resistor

// output pins
DDRD |= ((1 << PD0) | (1 << PD1) | (1 << PD3) | (1 << PD4));


// Variables
int count = 1;
    while(1)
    {

//Reading Switch and incrementing

if (PIND & (1 << PD6)) {}
        else {count++;}
           
//Reset if over 4
if (count == 5) {count = 1;}

// Switching Code

switch(count) {
      case 1 :
		OCR1A = 1; //1MHz
		TCCR1B = (1 << CS10) | (0 << CS11) | (0 << CS12) | (0 << WGM12) | (1 << WGM13);
		PORTD |=  (1 << PD0);           // switch PD0 LED on
         break;
      case 2 :
		OCR1A = 4; //250KHz
		TCCR1B = (1 << CS10) | (0 << CS11) | (0 << CS12) | (0 << WGM12) | (1 << WGM13);
		PORTD |=  (1 << PD1);           // switch PD1 LED on	
         break;
      case 3 :
		OCR1A = 100; //10KHz
		TCCR1B = (1 << CS10) | (0 << CS11) | (0 << CS12) | (0 << WGM12) | (1 << WGM13);
		PORTD |=  (1 << PD3);           // switch PD3 LED on
         break;
      case 4 :
		OCR1A = 972; //1Hz
		TCCR1B = (1 << CS10) | (0 << CS11) | (1 << CS12) | (0 << WGM12) | (1 << WGM13);
		PORTD |=  (1 << PD4);           // switch PD4 LED on
         break;
      default :
	break;

   }

_delay_ms(200); // giving you some time to release the switch

//Switching off all LEDs
PORTD &= ~(1 << PD0);           // switch PD0 LED off
PORTD &= ~(1 << PD1);           // switch PD0 LED off
PORTD &= ~(1 << PD3);           // switch PD0 LED off
PORTD &= ~(1 << PD4);           // switch PD0 LED off

    }
}
